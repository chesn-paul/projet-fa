#region
from tkinter import *
import pandas as pd 
import numpy as np
import math as mat
import os 
import plotly.graph_objects as go 
import plotly.express as px

#endregion

'''=====================================================================================
==================================== PARAMETRES ========================================
====================================================================================='''

'''-----------------------------------  GENERAL  ------------------------------------'''
#plage de temps à analyser : 
    #ENTRE 
mn = 0
sc = 0
#region
fro = str(mn) + "m" + str(sc)
#endregion

    #ET
mn = 10
sc = 0
#region
to = str(mn) + "m" + str(sc)
#endregion

#Si vol on analyse les données du vol précédent : 
buffer = False

#Echelles en km pour 1° de latitude ou de longitude :
dlat = 110.57
dlong = 78.85

'''---------------------------------  TRAJECTOIRE 3D  -------------------------------''' 
#Augmente l'écart entre les courbes de la trajectoire 3D :
demi_envergure = 20 

'''===================================================================================
======================================= CODE =========================================
==================================================================================='''

#PROCEDURE
#Proc pour trouver l'index dans les datas correspondant à un temps donné
def conmn(mil):
    heure = mil.split("m")
    mil = 0 
    mil = int(heure[0])*60000 + int(heure[1])*1000
    i = 0
    while dt[i,10] < mil and i < len(dt) - 1:
        i = i + 1
    return i

#Proc pour calculer les coordonées des bouts d'ailes de l'avion dans le repère en focntion du roulis (bleu/rouge)
def coord_rot(row):  

    dx = dt[row][0] - dt[row-1][0]
    dy = dt[row][1] - dt[row-1][1]
    dz = dt[row][2] - dt[row-1][2]

    m = mat.sqrt(pow(dx,2)+pow(dy,2)+pow(dz,2))

    if m == 0 : 
        ux = 0
        uy = 0 
        uz = 0
    else : 
        ux = dx/m
        uy = dy/m
        uz = dz/m

    rx = dt[row][4]
    c = mat.cos(mat.radians(rx))
    s = mat.sin(mat.radians(rx))
    
    rota = np.array([[((pow(ux,2)*(1-c))+c),((ux*uy*(1-c))-(uz*s)),((ux*uz*(1-c))+(uy*s))],
                    [((ux*uy*(1-c))+(uz*s)),((pow(uy,2)*(1-c))+c),((uy*uz*(1-c))-(ux*s))],
                    [((ux*uz*(1-c))-(uy*s)),((uy*uz*(1-c))+(ux*s)),((pow(uz,2)*(1-c))+c)]])

    ad = np.dot(rota,[-demi_envergure*(1-abs(ux)),-demi_envergure*(1-abs(uy)),-demi_envergure*(1-abs(uz))])
    ag = np.dot(rota,[ demi_envergure*(1-abs(ux)), demi_envergure*(1-abs(uy)), demi_envergure*(1-abs(uz))])

    rot = np.append(ad,ag)

    for i in range(0,3):
        rot[i] = rot[i] + dt[row][i]
        rot[i+3] = rot[i+3] + dt[row][i]

    for i in range(0,6):
        dt[row][i+11] = rot[i]


#MISE EN FORMES
#Analyse soit des data de l'avant dernier vol : buffer  ou du dernier vol : data
if buffer == False :
    dt = pd.read_csv(os.getcwd() + "\Data.csv", delimiter=';')
else : 
    dt = pd.read_csv(os.getcwd() + "\Buffer.csv", delimiter=';')

#Coordonnées du point de départ des mesures
xo = float(dt.iloc[0,0])
yo = float(dt.iloc[0,1])

#Remplacement des , des coordonnées GPS par des points + Déduction des coordonnées de départ des mesures
for i in range(len(dt.iloc[:,0])):
    dt.iloc[i,0] = (float(dt.iloc[i,0]) - xo) * dlat / 1000 
    dt.iloc[i,1] = (float(dt.iloc[i,1]) - yo) * dlong / 1000


#Mise en forme des données (->Arrray->Ajout de colonnes->Conversion en nb réels)
dt = dt.to_numpy()
dt = np.append(dt, np.zeros((len(dt),6)), axis=1)
dt = np.array(list(map(np.float_,dt)))

#Détermination des index pour les bornes de l'échantillon souhaité
fro = conmn(fro)
to = conmn(to)

#Détermination du maximum et du minimum des coordonnées GPS
maxi = int(dt[:,[1,0]].max()) + 100
mini = int(dt[:,[1,0]].min()) - 100

#Détermination du maximum et du minimum d'altitude
minia = int(min(dt[:,2]))
maxia = int(max(dt[:,2])) + 100

#Réduction des datas à la tailles de l'échantillon souhaité
dt = dt[fro:to,:]

#Pour toutes les datas calcul de la position du bout d'aile droite/gauche en fonction du degré de roulis
m = 0
n = 0
pt = 0
avg = 0 

for i in range(1,len(dt)):

    coord_rot(i)

    if dt[i-1,0] - dt[i,0] == 0 :
        m = m + 1 
    else :
        x_dif = dt[i-1,0] - dt[i,0]
        y_dif = dt[i-1,1] - dt[i,1]
        z_dif = dt[i-1,2] - dt[i,2]
        for k in range(m):
            dt[(i-m+k),0] = dt[(i-m+k),0] + (k+1)*(x_dif/m)
            dt[(i-m+k),1] = dt[(i-m+k),1] + (k+1)*(y_dif/m)
            dt[(i-m+k),2] = dt[(i-m+k),2] + (k+1)*(z_dif/m)
        m = 0

    if dt[pt,10] - dt[i,10] < 200 :
        n = n + 1
        avg = avg + dt[i,6]
    else : 
        avg = avg / n 
        for k in range(n):
            dt[(pt+k+1),6] = avg
        n = 0 
        avg = 0 
        pt = i

   

#Suppression première ligne des datas : point de départ des mesures
dt = np.delete(dt,0,0)

figtrois = go.Figure(data=[  go.Scatter3d(
                        x=dt[:,14],
                        y=dt[:,15],
                        z=dt[:,16],
                        mode='lines',
                        name='Aile Droite'),

                        go.Scatter3d(
                        x=dt[:,11],
                        y=dt[:,12],
                        z=dt[:,13],
                        mode='lines',
                        name='Aile Gauche'),

                        go.Scatter3d(
                        x=dt[:,0],
                        y=dt[:,1],
                        z=dt[:,2],
                        mode='lines',
                        name='Trajectoire',
                        marker=dict(
                            size=4,
                            color=dt[:,6],
                            colorscale='Plotly3'
                        ),
                        
                        line=dict(
                            color=dt[:,4],
                            colorscale='HSV',
                            width=4
                        ))
                    ])


frames = [go.Frame(data=[  
                    go.Scatter3d(
                    x=dt[0:k,14],
                    y=dt[0:k,15],
                    z=dt[0:k,16],
                    mode='lines',
                    name='Aile Droite'),

                    go.Scatter3d(
                    x=dt[0:k,11],
                    y=dt[0:k,12],
                    z=dt[0:k,13],
                    mode='lines',
                    name='Aile Gauche'),

                    go.Scatter3d(
                    x=dt[0:k,0],
                    y=dt[0:k,1],
                    z=dt[0:k,2],
                    mode='lines',
                    name='Trajectoire')

                    ])for k in range(len(dt[:,0]))]
                        
figtrois.update(frames=frames)

figtrois.update_layout(
    scene = dict(
        aspectmode='cube',
        xaxis = dict(backgroundcolor="rgba(0, 0, 0,0)",
                    gridcolor="lightgray",
                    showbackground=True,
                    zerolinecolor="lightgray",
                    range=[mini,maxi],
                    dtick = 100),

        yaxis = dict(backgroundcolor="rgba(0, 0, 0,0)",
                    gridcolor="lightgray",
                    showbackground=True,
                    zerolinecolor="lightgray",
                    range=[mini,maxi],
                    dtick = 100),

        zaxis = dict(backgroundcolor="rgba(0, 0, 0,0)",
                    gridcolor="lightgray",
                    showbackground=True,
                    zerolinecolor="lightgray",
                    range=[minia,maxia],
                    dtick = 100)
            ),

    updatemenus = [dict(
                    type="buttons",
                    buttons=[dict(label="Animation Trajectoire",
                                method="animate",
                                args=[None, dict(frame=dict(duration = 0, 
                                                        redraw = True,
                                                        fromcurrent = True, 
                                                        transition = 0))]),

                            dict(label="Ailes",
                                method = "update",
                                args=[{"visible": [True,True,False],
                                        "mode": "lines"}]),

                            dict(label="Trajectoire",
                                method = "update",
                                args=[{"visible": [False,False,True],
                                        "mode": "lines"}]),

                            dict(label="Ailes + trajectoire",
                                method = "update",
                                args=[{"visible": [True,True,True],
                                        "mode": "lines"}]),

                            dict(label="Animation facteur de Charge",
                                method="animate",
                                args=[{"visible": [False,False,True],
                                        "mode": "markers"}, 
                                        dict(frame=dict(duration = 0, 
                                            redraw = True,
                                            fromcurrent = True, 
                                            transition = 0))]),

                            dict(label="Facteur de charge",
                                method = "update",
                                args=[{"visible": [False,False,True],
                                        "mode": "markers"}])      
                    ])]
                
                    )

figtrois.show()

'''
#AFFICHAGE

Proc pour animer la trajectoire 3D
def animate (i):
    #ax.plot3D(dt[0:i,14], dt[0:i,15], dt[0:i,16], 'r', linewidth=0.75)
    #ax.plot3D(dt[0:i,11], dt[0:i,12], dt[0:i,13], 'b', linewidth=0.75)
    #ax.plot3D(dt[0:i,0], dt[0:i,1], dt[0:i,2], 'k', linewidth=0.25)

if trois_dim == True : 
    if anim == True : 
        #Graph animé
        ax = plt.figure(figsize = (10,8)).add_subplot(111, projection='3d')
        ax.set_xlim3d(mini, maxi)
        ax.set_ylim3d(mini, maxi)
        ax.set_zlim3d(minia, maxia)
        ax.grid(False)
        ani = FuncAnimation(plt.gcf(), animate, len(dt))
    else :
        if rub == False : 
            #Gaph deux lignes plus trajectoire
            ax = plt.figure(figsize = (10,8)).add_subplot(111, projection='3d')
            ax.plot(dt[:,14], dt[:,15], dt[:,16], 'r', linewidth=0.75)
            ax.plot(dt[:,11], dt[:,12], dt[:,13], 'b', linewidth=0.75)
            ax.plot(dt[:,0], dt[:,1], dt[:,2], 'k', linewidth=0.25)
            ax.set_xlim3d(mini, maxi)
            ax.set_ylim3d(mini, maxi)
            ax.set_zlim3d(minia, maxia)
            ax.grid(False)

        else : 
            #Graph f(facteur de charge)
            norm = plt.Normalize(vmin = dt[:,6].min(), vmax = dt[:,6].max())
            ax = plt.figure(figsize = (10,8)).add_subplot(111, projection='3d')
            ax.set_xlim3d(mini, maxi)
            ax.set_ylim3d(mini, maxi)
            ax.set_zlim3d(minia, maxia)
            ax.grid(False)
            ax.plot_surface(dt[:,[11,14]], dt[:,[12,15]], dt[:,[13,16]], facecolors = plt.cm.plasma(norm(dt[:,[6]])))
            m = mp.cm.ScalarMappable(cmap=plt.cm.plasma, norm=norm)
            plt.colorbar(m)
            
    plt.show()

if deux_dim == True : 
    #Graphs des paramètres
    for i in range(len(aff)):
        if aff[i][1] == True :
            color = [] 
            for j in range(3):
                color.append((random.randint(0,255)/255))
            color[random.randint(0,2)] = 0 
            fig, ax = plt.subplots()
            ax.plot(dt[:,10],dt[:,i+2], color=(color[0],color[1],color[2]))
            ax.set(xlabel="Temps(ms)", ylabel="Range(°;G;°/s)", title=aff[i][0])
            ax.grid(False)
    plt.show()'''