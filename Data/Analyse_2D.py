#region
import pandas as pd 
import numpy as np
import os 
import plotly.graph_objects as go 
import plotly.offline

#endregion

'''=====================================================================================
==================================== PARAMETRES ========================================
====================================================================================='''

'''-----------------------------------  GENERAL  ------------------------------------'''

#Si vol on analyse les données du vol précédent : 
buffer = False

'''===================================================================================
======================================= CODE =========================================
==================================================================================='''

#MISE EN FORMES
#Analyse soit des data de l'avant dernier vol : buffer  ou du dernier vol : data
if buffer == False :
    dt = pd.read_csv(os.getcwd() + "\Data.csv", delimiter=';')
else : 
    dt = pd.read_csv(os.getcwd() + "\Buffer.csv", delimiter=';')

#Mise en forme des données (->Arrray->Ajout de colonnes->Conversion en nb réels)
dt = dt.to_numpy()
dt = np.array(list(map(np.float_,dt)))

#Pour toutes les datas calcul de la position du bout d'aile droite/gauche en fonction du degré de roulis
n = 0
pt = 0
avg = 0 

for i in range(1,len(dt)):

    if dt[pt,10] - dt[i,10] < 200 :
        n = n + 1
        avg = avg + dt[i,6]
    else : 
        avg = avg / n 
        for k in range(n):
            dt[(pt+k+1),6] = avg
        n = 0 
        avg = 0 
        pt = i

#Suppression première ligne des datas : point de départ des mesures
dt = np.delete(dt,0,0)

figdeux = go.Figure(go.Scatter(
                    x=(dt[:,10] / 1000), 
                    y=dt[:,6], 
                    mode = "lines", 
                    line=dict(
                            color= "steelblue",
                            width=2)))

figdeux.update_layout(
    plot_bgcolor='rgba(0,0,0,0)', 

    xaxis = dict(
                gridcolor="lightgray",
                zerolinecolor="lightgray",
                dtick = 10,
                rangeslider=dict(
                    visible = True,
                )),

    yaxis = dict(
                gridcolor="lightgray",
                zerolinecolor="grey"),

    updatemenus = [dict(
                    type="buttons",
                    buttons=[
                            dict(label="Facteur de charge",
                                method = "update",
                                args=[{"y": [dt[:,6]]}]),

                            dict(label="Roulis",
                                method = "update",
                                args=[{"y": [dt[:,4]]}]),

                            dict(label="Tangage",
                                method = "update",
                                args=[{"y": [dt[:,3]]}]),

                            dict(label="Lacet",
                                method = "update",
                                args=[{"y": [dt[:,5]]}]),

                            dict(label="Vitesse roulis",
                                method = "update",
                                args=[{"y": [dt[:,8]]}]),

                            dict(label="Vitesse tangage",
                                method = "update",
                                args=[{"y": [dt[:,7]]}]),

                            dict(label="Vitesse lacet",
                                method = "update",
                                args=[{"y": [dt[:,9]]}])      
                    ])]
                )
figdeux.show()

'''
#AFFICHAGE

Proc pour animer la trajectoire 3D
def animate (i):
    #ax.plot3D(dt[0:i,14], dt[0:i,15], dt[0:i,16], 'r', linewidth=0.75)
    #ax.plot3D(dt[0:i,11], dt[0:i,12], dt[0:i,13], 'b', linewidth=0.75)
    #ax.plot3D(dt[0:i,0], dt[0:i,1], dt[0:i,2], 'k', linewidth=0.25)

if trois_dim == True : 
    if anim == True : 
        #Graph animé
        ax = plt.figure(figsize = (10,8)).add_subplot(111, projection='3d')
        ax.set_xlim3d(mini, maxi)
        ax.set_ylim3d(mini, maxi)
        ax.set_zlim3d(minia, maxia)
        ax.grid(False)
        ani = FuncAnimation(plt.gcf(), animate, len(dt))
    else :
        if rub == False : 
            #Gaph deux lignes plus trajectoire
            ax = plt.figure(figsize = (10,8)).add_subplot(111, projection='3d')
            ax.plot(dt[:,14], dt[:,15], dt[:,16], 'r', linewidth=0.75)
            ax.plot(dt[:,11], dt[:,12], dt[:,13], 'b', linewidth=0.75)
            ax.plot(dt[:,0], dt[:,1], dt[:,2], 'k', linewidth=0.25)
            ax.set_xlim3d(mini, maxi)
            ax.set_ylim3d(mini, maxi)
            ax.set_zlim3d(minia, maxia)
            ax.grid(False)

        else : 
            #Graph f(facteur de charge)
            norm = plt.Normalize(vmin = dt[:,6].min(), vmax = dt[:,6].max())
            ax = plt.figure(figsize = (10,8)).add_subplot(111, projection='3d')
            ax.set_xlim3d(mini, maxi)
            ax.set_ylim3d(mini, maxi)
            ax.set_zlim3d(minia, maxia)
            ax.grid(False)
            ax.plot_surface(dt[:,[11,14]], dt[:,[12,15]], dt[:,[13,16]], facecolors = plt.cm.plasma(norm(dt[:,[6]])))
            m = mp.cm.ScalarMappable(cmap=plt.cm.plasma, norm=norm)
            plt.colorbar(m)
            
    plt.show()

if deux_dim == True : 
    #Graphs des paramètres
    for i in range(len(aff)):
        if aff[i][1] == True :
            color = [] 
            for j in range(3):
                color.append((random.randint(0,255)/255))
            color[random.randint(0,2)] = 0 
            fig, ax = plt.subplots()
            ax.plot(dt[:,10],dt[:,i+2], color=(color[0],color[1],color[2]))
            ax.set(xlabel="Temps(ms)", ylabel="Range(°;G;°/s)", title=aff[i][0])
            ax.grid(False)
    plt.show()'''