import numpy as np
import math as mat
from numpy import genfromtxt
from numpy.linalg import inv

demi_envergure = 20 

dt = genfromtxt('E:\K+N\Projets\Book_data.csv', delimiter=';')

dt = np.delete(dt,0,axis=0)

def rotX(roulis):
    roulis = mat.radians(roulis)
    rotX = np.array([[1,0,0],
                    [0,mat.cos(roulis),-mat.sin(roulis)],
                    [0,mat.sin(roulis),mat.cos(roulis)]])
    return rotX

def rotY(tangage):
    tangage = mat.radians(tangage)
    rotY = np.array([[mat.cos(tangage),0,mat.sin(tangage)],
                    [0,1,0],
                    [-mat.sin(tangage),0,mat.cos(tangage)]])
    return rotY

def rotZ(lacet):
    lacet = mat.radians(lacet)
    rotZ = np.array([[mat.cos(lacet),-mat.sin(lacet),0],
                    [mat.sin(lacet), mat.cos(lacet),0],
                    [0,0,1]])
    return rotZ


def coord_rot(row):  
    rX = rotX(dt[row][3])
    rY = rotY(dt[row][4])
    rZ = rotZ(dt[row][5])

    ad = np.dot(rX,rY,rZ)
    ad=  np.dot(ad,np.array([0,-demi_envergure,-demi_envergure]))

    ag = np.dot(rX,rY,rZ)
    ag=  np.dot(ag,np.array([0,demi_envergure,demi_envergure]))

    rot = np.append(ad,ag)

    for i in range(0,3):
        rot[i] = rot[i] + dt[row][i]
        rot[i+3] = rot[i+3] + dt[row][i]

    for i in range(0,6):
        dt[row][i+7] = rot[i]
    
for i in range(0,len(dt)):
    coord_rot(i)

np.savetxt('E:\K+N\Projets\Book.csv', dt, delimiter=';',fmt='%.3f')




    