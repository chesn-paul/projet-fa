from tkinter import *
import sys
import time
global count
count =0
class stopwatch():
    def reset(self):
        global count
        count=1
        self.t.set('00:00:00')        
    def start(self):
        global count
        count=0
        self.timer()   
    def stop(self):
        global count
        count=1
    def close(self):
        self.root.destroy()
    def timer(self):
        global count
        if(count==0):
            self.d = str(self.t.get())
            h,m,s = map(int,self.d.split(":")) 
            h = int(h)
            m=int(m)
            s= int(s)
            if(s<59):
                s+=1
            elif(s==59):
                s=0
                if(m<59):
                    m+=1
                elif(m==59):
                    m=0
                    h+=1
            if(h<10):
                h = str(0)+str(h)
            else:
                h= str(h)
            if(m<10):
                m = str(0)+str(m)
            else:
                m = str(m)
            if(s<10):
                s=str(0)+str(s)
            else:
                s=str(s)
            self.d=h+":"+m+":"+s           
            self.t.set(self.d)
            if(count==0):
                self.root.after(1000,self.timer)  

    def __init__(self):
        self.root=Tk()
        self.root.title("Stop Watch")
        window_width = 110
        window_height = 30
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        center_x = int(screen_width - window_width )
        center_y = int(screen_height - 70)
        self.root.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        self.t = StringVar()
        self.t.set("00:00:00")
        self.root.attributes('-topmost',True)
        self.root.overrideredirect(True)
        self.lb = Label(self.root,textvariable=self.t,font=("Tw 15"),bg="black", fg="white")
        self.timer()
        self.bt4 = Button(self.root, text="X", command=self.close,font=("Tw 10 bold"), bg="black", fg="red", highlightthickness = 0, bd = 0)
        self.lb.place(x=0,y=0)
        self.bt4.place(x=85,y=5)
        self.label = Label(self.root,text="",font=("Times 40 bold"))
        self.root.configure(bg='black')
        self.root.mainloop()

a = stopwatch()