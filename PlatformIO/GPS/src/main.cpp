#include <Wire.h>
#include <TinyGPS++.h>



#define RXD2 16
#define TXD2 17
HardwareSerial neogps(1);

TinyGPSPlus gps;

void setup() {
  Serial.begin(115200);
  //Begin serial communication Arduino IDE (Serial Monitor)

  //Begin serial communication Neo6mGPS
  neogps.begin(9600, SERIAL_8N1, RXD2, TXD2);

}

float test;

void loop() {

    while (neogps.available())
    {
      if (gps.encode(neogps.read()))
      { 
        Serial.println(gps.location.lat());
        Serial.println(gps.location.lng());
        Serial.println(gps.altitude.value());
      }
    }

}
