#include <Arduino.h>
#include <MPU6050_light.h>
#include <Wire.h>
#include <FS.h>
#include <SD.h>
#include <SPI.h>
#include <iostream>
#include <TinyGPS++.h>

#define RXD2 16
#define TXD2 17

MPU6050 mpu(Wire);
HardwareSerial neogps(1);
TinyGPSPlus gps;

String cont;
String buffer;
 
int i;
int top;

long int t;
long int l;
long int p;
 
float old[7][5];
float sum[7];
float data[7];


void writeFile(fs::FS &fs, const char *path, const char *message)
{
    Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)){
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char *path, String message)
{

  File file = fs.open(path, FILE_APPEND);

  if (!file)
  {
    Serial.println("Failed to open file for appending");
    return;
  }

  if (file.print(message))
  {
  }
  else
  {
    Serial.println("Append failed");
  }

  file.close();
}

void renameFile(fs::FS &fs, const char *path1, const char *path2)
{

  if (fs.rename(path1, path2))
  {
  }
  else
  {
    Serial.println("Rename failed");
  }
}

void deleteFile(fs::FS &fs, const char *path)
{

  if (fs.remove(path))
  {
  }
  else
  {
    Serial.println("Delete failed");
  }
}

void setup()
{ 
  pinMode(2, OUTPUT);
  digitalWrite(2,HIGH);

  Serial.begin(115200);
  Wire.begin();
  neogps.begin(9600, SERIAL_8N1, RXD2, TXD2);

  while(!SD.begin(5)){
    digitalWrite(2,LOW);
  }

  uint8_t cardType = SD.cardType();
  Serial.println(cardType);

  while(cardType == CARD_NONE){
    digitalWrite(2,LOW);
  }

  byte status = mpu.begin();

  while(status!=0){
    digitalWrite(2,LOW);
  } 

  deleteFile(SD, "/Buffer.csv");
  renameFile(SD, "/Data.csv", "/Buffer.csv");
  writeFile(SD, "/Data.csv", "");

  mpu.calcOffsets(true, true);

  for (int i = 0; i < 5; i++) {
    digitalWrite(2,LOW);
    delay(1000);
    digitalWrite(2,HIGH);
    delay(1000);
  }
  
  t = millis();
  l = millis();

}

/*--------------------------------------------------------LOOP-------------------------------------------------------------*/
void loop()
{

  mpu.update();

  if (neogps.available()){
    if (gps.encode(neogps.read())){
      data[0] = gps.location.lat()*1000000;
      data[1] = gps.location.lng()*1000000;
      data[2] = gps.altitude.value();
    }
  }

  data[3] = mpu.getAngleX();
  data[4] = mpu.getAngleY();
  data[5] = mpu.getAngleZ();
  data[6] = pow(pow(mpu.getAccX(), 2) + pow(mpu.getAccY(), 2) + pow(mpu.getAccZ(), 2), 0.5);
  data[7] = mpu.getGyroX();
  data[8] = mpu.getGyroY();
  data[9] = mpu.getGyroZ();
  
  cont = "";
  for ( i=0 ; i<10 ; i++){
    cont = cont + String(data[i]) + ";";
  }
  

  cont = cont + millis() + "\n";
  appendFile(SD, "/Data.csv", cont.c_str());
  
  Serial.println(cont);

  if (data[0] == 0){
    digitalWrite(2,HIGH);
  }else{
    digitalWrite(2,LOW);
  }
  

}